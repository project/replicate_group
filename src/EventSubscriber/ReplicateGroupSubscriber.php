<?php

/**
 * @file
 * Contains \Drupal\replicate\EventSubscriber\ReplicateGroupSubscriber.
 */

namespace Drupal\replicate_group\EventSubscriber;

use Drupal\group\Entity\GroupContentInterface;
use Drupal\replicate\Events\ReplicateAlterEvent;
use Drupal\replicate\Replicator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\replicate\Events\ReplicatorEvents;

/**
 * Class ReplicateGroupSubscriber.
 *
 * @package Drupal\replicate\EventSubscriber
 */
class ReplicateGroupSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\replicate\Replicator
   */
  private $replicator;

  public function __construct(Replicator $replicator) {
    $this->replicator = $replicator;
  }

  /**
   * Replicate all content to the newly replicated group.
   *
   * @param \Drupal\replicate\Events\ReplicateAlterEvent $event
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onReplicateAlter(ReplicateAlterEvent $event) {
    $original = $event->getOriginal();

    if ($original->getEntityTypeId() == 'group') {
      $group = $event->getEntity();
      $group->save();
      $group_contents = $original->getContent();
      foreach ($group_contents as $group_content) {
        if ($group_content->getEntity()->getEntityTypeId() == 'node') {
          $entity = $this->replicateGroupContent($group_content);
          $group->addContent($entity, $group_content->getContentPlugin()
            ->getPluginId());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ReplicatorEvents::REPLICATE_ALTER][] = 'onReplicateAlter';
    return $events;
  }

  private function replicateGroupContent(GroupContentInterface $group_content) {
    $original_entity = $group_content->getEntity();
    $entity = $this->replicator->cloneEntity($original_entity);
    $entity->save();
    return $entity;
  }

}
